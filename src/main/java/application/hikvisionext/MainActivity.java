package application.hikvisionext;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;
import library.hikvisionext.HveActivateInfo;
import library.hikvisionext.HveAudio;
import library.hikvisionext.HveBrowser;
import library.hikvisionext.HveClient;
import library.hikvisionext.HveEndpoint;
import library.hikvisionext.HveGrid;
import library.hikvisionext.HveIPAddress;
import library.hikvisionext.HveIPv6Mode;
import library.hikvisionext.HveLayout;
import library.hikvisionext.HveMotionDetection;
import library.hikvisionext.HveMotionDetectionLayout;
import library.hikvisionext.HveStreamingChannel;
import library.hikvisionext.HveVideo;
import library.hikvisionext.HveWPA;
import library.hikvisionext.HveWireless;
import library.hikvisionext.HveWirelessSecurity;

public class MainActivity extends AppCompatActivity implements HveBrowser.Listener {
    HveBrowser browser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        HveClient client = HveClient.client("http://192.168.8.1", "admin", "qwerty-123");
//        activateV1(client);
//        wirelessUpdate(client);
//        ipAddressUpdate(client);
//        deviceInfoGet(client);
//        streamingChannelUpdate(client);
//        timeUpdateV1(client);
//        motionDetectionUpdate(client);

        browser = HveBrowser.browser(this, "_psia._tcp");
        browser.listeners.add(this);
        browser.start();
    }

    @Override
    public void endpointFound(HveBrowser sender, HveEndpoint endpoint) {
        sender.stop();

        HveClient client = HveClient.client(endpoint, "admin", "qwerty-123");
//        activateV1(client);
//        wirelessUpdate(client);
//        ipAddressUpdate(client);
//        deviceInfoGet(client);
//        streamingChannelUpdate(client);
//        timeUpdateV1(client);
        motionDetectionUpdate(client);
    }

    @Override
    public void endpointRemoved(HveBrowser sender, String name) {
        Toast.makeText(this, "endpointRemoved", Toast.LENGTH_SHORT).show();
    }

    public void activateV1(HveClient client) {
        HveActivateInfo activateInfo = new HveActivateInfo();
        activateInfo.password = "qwerty-123";

        client.activateV1Async(activateInfo, (responseStatus, exception) -> {
            if (responseStatus == null) {
                Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void wirelessUpdate(HveClient client) {
        HveWPA wpa = new HveWPA();
        wpa.algorithmType = "TKIP";
        wpa.sharedKey = "12345678";
        wpa.wpaKeyLength = wpa.sharedKey.length();

        HveWirelessSecurity wirelessSecurity = new HveWirelessSecurity();
        wirelessSecurity.securityMode = "WPA2-personal";
        wirelessSecurity.wpa = wpa;

        HveWireless wireless = new HveWireless();
        wireless.enabled = true;
        wireless.ssid = "My network";
        wireless.wirelessSecurity = wirelessSecurity;

        client.wirelessUpdateAsync("2", wireless, (responseStatus, exception) -> {
            if (responseStatus == null) {
                Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void ipAddressUpdate(HveClient client) {
        HveIPv6Mode ipv6Mode = new HveIPv6Mode();
        ipv6Mode.ipv6AddressingType = "dhcp";

        HveIPAddress ipAddress = new HveIPAddress();
        ipAddress.ipVersion = "dual";
        ipAddress.addressingType = "dynamic";
        ipAddress.ipv6Mode = ipv6Mode;

        client.ipAddressUpdateAsync("1", ipAddress, (responseStatus, exception) -> {
            if (responseStatus == null) {
                Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            } else {
                client.rebootAsync(null);
                Toast.makeText(this, "Rebooting...", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void deviceInfoGet(HveClient client) {
        client.deviceInfoGetAsync((deviceInfo, exception) -> {
            if (deviceInfo == null) {
                Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, deviceInfo.model, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void streamingChannelUpdate(HveClient client) {
        HveVideo video = new HveVideo();
        video.videoCodecType = "H.264";
        video.videoResolutionWidth = 1920;
        video.videoResolutionHeight = 1080;
        video.videoQualityControlType = "VBR";
        video.maxFrameRate = 2500;

        HveAudio audio = new HveAudio();
        audio.enabled = true;
        audio.audioCompressionType = "G.726";

        HveStreamingChannel streamingChannel = new HveStreamingChannel();
        streamingChannel.video = video;
        streamingChannel.audio = audio;

        client.streamingChannelUpdateAsync("101", streamingChannel, (responseStatus, exception) -> {
            if (responseStatus == null) {
                Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, video.videoCodecType, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void timeUpdateV1(HveClient client) {
        client.timeUpdateV1Async((responseStatus, exception) -> {
            if (responseStatus == null) {
                Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Time updated", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void motionDetectionUpdate(HveClient client) {
        HveGrid grid = new HveGrid();
        grid.rowGranularity = 18;
        grid.columnGranularity = 22;

        HveLayout layout = new HveLayout();
        layout.gridMap = "fffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffc";

        HveMotionDetectionLayout motionDetectionLayout = new HveMotionDetectionLayout();
        motionDetectionLayout.sensitivityLevel = 40;
        motionDetectionLayout.layout = layout;

        HveMotionDetection motionDetection = new HveMotionDetection();
        motionDetection.enabled = false;
        motionDetection.grid = grid;
        motionDetection.motionDetectionLayout = motionDetectionLayout;

        client.motionDetectionUpdateAsync("1", motionDetection, (responseStatus, exception) -> {
            if (responseStatus == null) {
                Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Motion detection disabled", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
